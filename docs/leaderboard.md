# Leaderboard
Returns a list of top players or bands.

### Player

Attributes:

| Name | Type |
|------|------|
| id.high | int |
| id.low | int |
| tag | str |
| name | str |
| position_in_leaderboard | int |
| trophies | int |
| band_name | str |
| exp_level | int |

### Band

Attributes:

| Name | Type |
|------|------|
| id.high | int |
| id.low | int |
| tag | str |
| name | str |
| position_in_leaderboard | int |
| trophies | int |
| members_count | int |
